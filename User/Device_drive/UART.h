
#ifndef _UART_H_
#define _UART_H_

#include "stm32f10x.h"
#include "stdio.h"
/*=====================================================================================================*/
/*                      USART1                                                                          */
/*=====================================================================================================*/
#define USARTx1                USART1
#define USART1_CLK            RCC_APB2Periph_USART1

#define USART1_TX_PIN         GPIO_Pin_9
#define USART1_TX_GPIO_PORT   GPIOA
#define USART1_TX_GPIO_CLK    RCC_APB2Periph_GPIOA
#define USART1_TX_MODE        GPIO_Mode_AF_PP

#define USART1_RX_PIN         GPIO_Pin_10
#define USART1_RX_GPIO_PORT   GPIOA
#define USART1_RX_GPIO_CLK    RCC_APB2Periph_GPIOA
#define USART1_RX_MODE        GPIO_Mode_IN_FLOATING

#define USART1_BAUDRATE       57600
#define USART1_BYTESIZE       USART_WordLength_8b
#define USART1_STOPBITS       USART_StopBits_1
#define USART1_PARITY         USART_Parity_No
#define USART1_Mode           (USART_Mode_Rx | USART_Mode_Tx)
#define USART1_HARDWARECTRL   USART_HardwareFlowControl_None
/*=====================================================================================================*/
/*                      USART2                                                                          */
/*=====================================================================================================*/
#define USARTx2               USART2
#define USART2_CLK            RCC_APB1Periph_USART2

#define USART2_TX_PIN         GPIO_Pin_2
#define USART2_TX_GPIO_PORT   GPIOA
#define USART2_TX_GPIO_CLK    RCC_APB2Periph_GPIOA
#define USART2_TX_MODE        GPIO_Mode_AF_PP

#define USART2_RX_PIN         GPIO_Pin_3
#define USART2_RX_GPIO_PORT   GPIOA
#define USART2_RX_GPIO_CLK    RCC_APB2Periph_GPIOA
#define USART2_RX_MODE        GPIO_Mode_IN_FLOATING

#define USART2_BAUDRATE       57600
#define USART2_BYTESIZE       USART_WordLength_8b
#define USART2_STOPBITS       USART_StopBits_1
#define USART2_PARITY         USART_Parity_No
#define USART2_Mode           (USART_Mode_Rx | USART_Mode_Tx)
#define USART2_HARDWARECTRL   USART_HardwareFlowControl_None
/*=====================================================================================================*/
/*                      USART3                                                                          */
/*=====================================================================================================*/
#define USARTx3               USART3
#define USART3_CLK            RCC_APB1Periph_USART3

#define USART3_TX_PIN         GPIO_Pin_10
#define USART3_TX_GPIO_PORT   GPIOB
#define USART3_TX_GPIO_CLK    RCC_APB2Periph_GPIOB
#define USART3_TX_MODE        GPIO_Mode_AF_PP

#define USART3_RX_PIN         GPIO_Pin_11
#define USART3_RX_GPIO_PORT   GPIOB
#define USART3_RX_GPIO_CLK    RCC_APB2Periph_GPIOB
#define USART3_RX_MODE        GPIO_Mode_IN_FLOATING

#define USART3_BAUDRATE       115200
#define USART3_BYTESIZE       USART_WordLength_8b
#define USART3_STOPBITS       USART_StopBits_1
#define USART3_PARITY         USART_Parity_No
#define USART3_Mode           (USART_Mode_Rx | USART_Mode_Tx)
#define USART3_HARDWARECTRL   USART_HardwareFlowControl_None
/*=====================================================================================================*/
/*=====================================================================================================*/
uint8_t usart1_tx_ringbuffer_push(const uint8_t* ch, uint8_t len);
int usart1_char_available(void);
uint8_t usart1_rx_ringbuffer_pop(void);
uint8_t usart1_rx_ringbuffer_push_from_usart(void);
uint8_t usart1_tx_ringbuffer_pop_to_usart(void);
uint8_t usart2_tx_ringbuffer_push(const uint8_t* ch, uint8_t len);
int usart2_char_available(void);
uint8_t usart2_rx_ringbuffer_pop(void);
uint8_t usart2_rx_ringbuffer_push_from_usart(void);
uint8_t usart2_tx_ringbuffer_pop_to_usart(void);
void UART1Config(void);
void UART1NVIC_Configuration(void);
void UART2Config(void);
void UART2NVIC_Configuration(void);
void UART3Config(void);
void UART3NVIC_Configuration(void);
int fputc(int ch, FILE *f);

#endif

