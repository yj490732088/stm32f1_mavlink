#include "mavlinktask.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
unsigned long int mav_send_frequency = 0;
portTickType  Mav_lastFlashTime = 0;
void Task_MavLink(void *pvParameters)
{ 
  portBASE_TYPE xStatus; 
	const portTickType xTicksToWait=0/portTICK_RATE_MS;

	Mav_lastFlashTime=xTaskGetTickCount();
	
	for(;;)
	{
	 vTaskDelayUntil(&Mav_lastFlashTime,(portTickType)(5/portTICK_RATE_MS)); 
	  mav_send_frequency++;
		communication_receive();
//   if(mav_send_frequency % 5 == 0)
//   communication_system_state_send();
	}
}