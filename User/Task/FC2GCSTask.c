#include "FC2GCSTask.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
unsigned long int fc2gcs_send_frequency = 0;
portTickType  fc2gcs_lastFlashTime = 0;
void Task_FC2GCS(void *pvParameters)
{ 
  portBASE_TYPE xStatus; 
	const portTickType xTicksToWait=0/portTICK_RATE_MS;

	fc2gcs_lastFlashTime=xTaskGetTickCount();
	
	for(;;)
	{
	 vTaskDelayUntil(&fc2gcs_lastFlashTime,(portTickType)(5/portTICK_RATE_MS)); 
	  fc2gcs_send_frequency++;
		communication_receive();
	}
}