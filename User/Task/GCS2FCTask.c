#include "GCS2FCTask.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
unsigned long int gcs2fc_send_frequency = 0;
portTickType  gcs2fc_lastFlashTime = 0;
void Task_GCS2FC(void *pvParameters)
{ 
  portBASE_TYPE xStatus; 
	const portTickType xTicksToWait=0/portTICK_RATE_MS;

	gcs2fc_lastFlashTime=xTaskGetTickCount();
	
	for(;;)
	{
	 vTaskDelayUntil(&gcs2fc_lastFlashTime,(portTickType)(5/portTICK_RATE_MS)); 
	  gcs2fc_send_frequency++;
		communication_receive();
	}
}